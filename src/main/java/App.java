import com.google.gson.Gson;
import model.Car;

/*
    TO FIX IMPORT FOR GOOGLE GSON
    SEE: https://stackoverflow.com/questions/1051640/correct-way-to-add-external-jars-lib-jar-to-an-intellij-idea-project/1051705#1051705
*/
public class App {
    public static void main(String[] args) {
        // source: http://tutorials.jenkov.com/java-json/gson.html
        String json = "{\"brand\":\"Jeep\", \"doors\": 3}";
        Gson gson = new Gson();
        Car car = gson.fromJson(json, Car.class);
        System.out.println(car);
    }
}
